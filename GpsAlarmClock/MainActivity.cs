﻿using Android.App;
using Android.Content;
using Android.Gms.Maps;
using Android.Gms.Maps.Model;
using Android.OS;
using Android.Widget;
using GpsAlarmClock.Services;

namespace GpsAlarmClock
{
    [Activity(Label = "GpsAlarmClock", MainLauncher = true)]
    public class MainActivity : Activity, IOnMapReadyCallback
    {
        private MapFragment _mapFragment;
        private GpsService _gpsService;

        public void OnMapReady(GoogleMap googleMap)
        {
            if (googleMap != null && _gpsService.IsLocationAvailable())
            {
                var gps = _gpsService.GetCurrentLocation().Result;
                System.Console.WriteLine($"Coordinates are: [Lat] {gps.Latitude} [Long] {gps.Longitude}");
                var builder = CameraPosition.InvokeBuilder();
                var positionLatLng = new LatLng(gps.Latitude, gps.Longitude);
                builder.Target(positionLatLng);
                builder.Zoom(1);

                var position = builder.Build();
                var cameraUpdate = CameraUpdateFactory.NewCameraPosition(position);

                var currentLoc = new MarkerOptions();
                currentLoc.SetPosition(positionLatLng);

                googleMap.AddMarker(currentLoc);
                googleMap.MoveCamera(cameraUpdate);
            }

            var translationHistoryButton = FindViewById<Button>(Resource.Id.settingLayoutButton);

            translationHistoryButton.Click += (sender, e) =>
            {
                var intent = new Intent(this, typeof(SettingLayoutActivity));
                System.Console.WriteLine("Test");
                StartActivity(intent);
            };

            ApplicationSettings.Map = googleMap;
        }

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            // GPS Service init
            _gpsService = new GpsService();

            SetContentView(Resource.Layout.MapWithOverlayLayout);

            _mapFragment = FragmentManager.FindFragmentByTag("map") as MapFragment;
            if (_mapFragment == null)
            {
                GoogleMapOptions mapOptions = new GoogleMapOptions()
                    .InvokeMapType(GoogleMap.MapTypeNormal)
                    .InvokeZoomControlsEnabled(true)
                    .InvokeCompassEnabled(true);

                FragmentTransaction fragTx = FragmentManager.BeginTransaction();
                _mapFragment = MapFragment.NewInstance(mapOptions);
                fragTx.Add(Resource.Id.mapWithOverlay, _mapFragment, "map");
                fragTx.Commit();
            }
            _mapFragment.GetMapAsync(this);

            // Set our view from the "main" layout resource
            // SetContentView(Resource.Layout.Main);
        }
    }
}

