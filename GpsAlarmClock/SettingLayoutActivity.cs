﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Support.Design.Widget;
using Android.Views;
using Android.Widget;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

namespace GpsAlarmClock
{
    [Activity(Label = "@string/settingLayout")]
    public class SettingLayoutActivity : Activity
    {
        private EditText distanceInput;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            Console.WriteLine("button was clicked");
            SetContentView(Resource.Layout.Settings);

            distanceInput = FindViewById<EditText>(Resource.Id.distanceText);
            distanceInput.Text = ApplicationSettings.AlarmDistanceKm.ToString();
            distanceInput.TextChanged += OnInputChange;

            var backButton = FindViewById<Android.Widget.Button>(Resource.Id.backButton);
            backButton.Click += BackButton_Click;
        }

        private void BackButton_Click(object sender, EventArgs e)
        {
            var intent = new Intent(this, typeof(MainActivity));
            System.Console.WriteLine("Back to Main");
            StartActivity(intent);
        }

        private void OnInputChange(object sender, Android.Text.TextChangedEventArgs e)
        {
            double d;
            if (double.TryParse(e.Text.ToString(), out d))
            {
                ApplicationSettings.AlarmDistanceKm = d;
            }
            else
            {
                // Remove from event list to be aware of loop
                distanceInput.TextChanged -= OnInputChange;
                // Rollback to previous value
                distanceInput.Text = ApplicationSettings.AlarmDistanceKm.ToString();
                // Put back to event list
                distanceInput.TextChanged += OnInputChange;
            }
            ApplicationSettings.DebugOutput();
        }
    }
}