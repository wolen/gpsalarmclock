﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.Gms.Maps;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace GpsAlarmClock
{
    public static class ApplicationSettings
    {
        public static double AlarmDistanceKm;
        public static GoogleMap Map;

        public static void DebugOutput()
        {
            Console.WriteLine(nameof(AlarmDistanceKm) + $" value: {AlarmDistanceKm}.");
        }
    }
}