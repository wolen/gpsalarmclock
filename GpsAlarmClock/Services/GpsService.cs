﻿using Plugin.Geolocator;
using System.Threading.Tasks;
using Plugin.Geolocator.Abstractions;
using Android.Gms.Maps.Model;
using System.Diagnostics;
using System;

namespace GpsAlarmClock.Services
{
    class GpsService
    {
        IGeolocator _locator;
        Position _cachedPosition = null;

        public GpsService()
        {
            _locator = CrossGeolocator.Current;

            // Default position is Tabor
            _cachedPosition = new Position(49.412640, 14.664302);
        }

        public async Task<LatLng> GetCurrentPosition()
        {
            var position = await _locator.GetPositionAsync(timeout: TimeSpan.FromMilliseconds(10000));

            var lanLtg = new LatLng(position.Latitude, position.Longitude);

            return lanLtg;
        }

        public bool IsLocationAvailable()
        {
            if (!CrossGeolocator.IsSupported)
            {
                return false;
            }

            return _locator.IsGeolocationAvailable;
        }

        public async Task<Position> GetCurrentLocation()
        {
            Position position = null;
            try
            {
                var locator = CrossGeolocator.Current;
                locator.DesiredAccuracy = 100;

                position = await locator.GetLastKnownLocationAsync();

                if (position != null)
                {
                    //got a cahched position, so let's use it.
                    _cachedPosition = position;

                    return _cachedPosition;
                }

                if (!locator.IsGeolocationAvailable || !locator.IsGeolocationEnabled)
                {
                    //not available or enabled
                    return _cachedPosition;
                }

                position = await locator.GetPositionAsync(TimeSpan.FromSeconds(20), null, true);

            }
            catch (Exception)
            {
                // Display error as we have timed out or can't get location.
            }

            if (position == null)
            {
                return _cachedPosition;
            }

            return _cachedPosition;
        }
    }
}